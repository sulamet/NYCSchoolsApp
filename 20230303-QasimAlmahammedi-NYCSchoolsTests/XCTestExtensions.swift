//
//  XCTestExtensions.swift
//  20230303-QasimAlmahammedi-NYCSchoolsTests
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import Foundation
import XCTest

extension XCTest {
    func openJsonFile<ModelType: Decodable>(
        named name: String,
        file: StaticString = #file,
        line: UInt = #line) throws -> ModelType {

            let bundle = Bundle(for: type(of: self))
            let url = try XCTUnwrap(bundle.url(forResource: name, withExtension: "json"))
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            return try decoder.decode(ModelType.self, from: data)
        }
}
