//
//  SchoolsTests.swift
//  20230303-QasimAlmahammedi-NYCSchoolsTests
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import XCTest
@testable import _0230303_QasimAlmahammedi_NYCSchools

// MARK: - Top Schools Tests
class SchoolsTests: XCTestCase {
    func testSchoolsDecoding() throws {
        let allSchools: [School] = try openJsonFile(named: "schools")
        let school = allSchools[1]
        XCTAssertEqual(school.id, "21K728")
        XCTAssertEqual(school.name, "Liberation Diploma Plus High School")
        XCTAssertEqual(school.phone, "718-946-6812")
        XCTAssertEqual(school.location, "2865 West 19th Street, Brooklyn, NY 11224 (40.576976, -73.985413)")
    }

}

// MARK: - Single School Tests
class SingleSchoolTests: XCTestCase {
    func testSingleSchoolDecoding() throws {
        let details: [School.Details] = try openJsonFile(named: "single")
        let school = try XCTUnwrap(details.first)
        XCTAssertEqual(school.id, "21K728")
        XCTAssertEqual(school.name, "LIBERATION DIPLOMA PLUS")
        XCTAssertEqual(school.testTakers, "10")
        XCTAssertEqual(school.readingScore, "411")
        XCTAssertEqual(school.mathScore, "369")
        XCTAssertEqual(school.writingScore, "373")
    }
}
