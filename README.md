Main NYC Schools App Features:
 - Well architected app, all based on SwiftUI. Tree-shaped extended MVVM design pattern used.
 - Unit Testing included
 - Global shared state for the entire app via State Controller
 - Themable: Basic complementary  themes included to be set by the user via Settings Controller
 - Offline Mode: App persists schools list on disk via Storage Controller. Storage solution with global shared access is included.
 - Many, many more features, helpful and generic extensions and custom SwiftUI and view modifiers are added.
 - Complementary Login Screen added. No password required. Only shows the first time the App is launched.
 - Custom Loading Indicator added. Basic error handling with alert covered.

Sample App:

 ![Sample App Preview](https://s2.gifyu.com/images/Simulator-Screen-Recording---iPhone-13---2023-03-16-at-11.52.58.gif)
