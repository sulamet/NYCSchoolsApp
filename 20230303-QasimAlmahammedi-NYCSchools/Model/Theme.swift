//
//  Theme.swift
//  20230303-QasimAlmahammedi-NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import Foundation
import SwiftUI

// MARK: - Theme
struct Theme: Identifiable {
    let name: String
    let accentColor: Color
    let secondaryColor: Color
    let primaryGradient: LinearGradient
    let secondaryGradient: LinearGradient

    var id: String { name }

    static let `default` = Theme(
        name: "Default",
        accentColor: .blue,
        secondaryColor: .orange,
        primaryGradient: .blue,
        secondaryGradient: .orange)

    static let web = Theme(
        name: "Web",
        accentColor: .teal,
        secondaryColor: .green,
        primaryGradient: .teal,
        secondaryGradient: .green)

    static let allThemes: [Theme] = [.default, .web]

    static func named(_ name: String) -> Theme? {
        allThemes.first(where: { $0.name == name })
    }
}

// MARK: - ThemeKey
struct ThemeKey: EnvironmentKey {
    static let defaultValue = Theme.default
}

// MARK: - EnvironmentValues
extension EnvironmentValues {
    var theme: Theme {
        get { self[ThemeKey.self] }
        set { self[ThemeKey.self] = newValue  }
    }
}

// MARK: - LinearGradient
extension LinearGradient {
    static var blue: Self { verticalGradient(with: [.lightBlue, .blue]) }
    static var orange: Self { verticalGradient(with: [.lightOrange, .orange]) }
    static var green: Self { verticalGradient(with: [.lightGreen, .green]) }
    static var teal: Self { verticalGradient(with: [.lightTeal, .teal]) }

    private static func verticalGradient(with colors: [Color]) -> Self {
        let gradient = Gradient(colors: colors)
        return LinearGradient(
            gradient: gradient,
            startPoint: UnitPoint(x: 0.0, y: 0.0),
            endPoint: UnitPoint(x: 0.0, y: 1.0))
    }
}

// MARK: - Color
extension Color {
    static var lightBlue: Color {
        Color(UIColor.systemBlue.colorWithOffsets(saturation: -0.5))
    }

    static var lightOrange: Color {
        Color(UIColor.systemOrange.colorWithOffsets(saturation: -0.5))
    }

    static var lightGreen: Color {
        Color(UIColor.systemGreen.colorWithOffsets(saturation: -0.15, brightness: 0.2))
    }

    static var lightTeal: Color {
        Color(UIColor.systemTeal.colorWithOffsets(saturation: -0.5, brightness: 0.2))
    }
}

// MARK: - UIColor
extension UIColor {
    var hsba: (hue: CGFloat, saturation: CGFloat, brightness: CGFloat, alpha: CGFloat) {
        var hue: CGFloat = 0.0
        var saturation: CGFloat = 0.0
        var brightness: CGFloat = 0.0
        var alpha: CGFloat = 0.0
        self.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        return (hue, saturation, brightness, alpha)
    }

    func colorWithOffsets(
        hue: CGFloat = 0.0,
        saturation: CGFloat = 0.0,
        brightness: CGFloat = 0.0,
        alpha: CGFloat  = 0.0) -> UIColor {
            UIColor(
                hue: hsba.hue + hue,
                saturation: hsba.saturation + saturation,
                brightness: hsba.brightness + brightness,
                alpha: hsba.alpha + alpha
            )
        }
}

