//
//  Helpers.swift
//  NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import Foundation
import SwiftUI

// MARK: URL extension
extension URL {
    func appendingParameters(_ parameters: [String: String]) -> URL {
        var urlComponents = URLComponents(url: self, resolvingAgainstBaseURL: false)!
        var queryItems = urlComponents.queryItems ?? []
        for key in parameters.keys {
            queryItems.append(URLQueryItem(name: key, value: parameters[key]))
        }
        urlComponents.queryItems = queryItems
        return urlComponents.url!
    }
}

// MARK: - Wrapper around the whole JSON object
// MARK: - Wrapper
struct Wrapper<ModelType: Decodable>: Decodable {
    let items: [ModelType]

    enum CodingKeys: String, CodingKey {
        case items
    }
}

// MARK: - Preview extension
extension View {
    func previewWithName(_ name: String) -> some View {
        self
            .padding()
            .previewLayout(.sizeThatFits)
            .previewDisplayName(name)
    }

    func namedPreview() -> some View {
        let name = String.name(for: type(of: self))
        return previewWithName(name)
    }
}

// MARK: - String
extension String {
    static func name<T>(for type: T) -> String {
        String(reflecting: type)
            .components(separatedBy: ".")
            .last ?? ""
    }
}
