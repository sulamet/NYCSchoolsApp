//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    @StateObject private var stateController = StateController()
    @StateObject private var settingsController = SettingsController()

    var body: some Scene {
        WindowGroup {
            MainContentView()
                .environmentObject(stateController)
                .environmentObject(settingsController)
        }
    }
}


extension NYCSchoolsApp {
    struct Keys {
        static let themeName = "ThemeName"
        static let isLoggedIn = "IsLoggedIn"
    }
}
