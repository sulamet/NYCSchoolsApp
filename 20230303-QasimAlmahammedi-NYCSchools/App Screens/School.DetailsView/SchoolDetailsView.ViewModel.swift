//
//  SchoolDetailsView.ViewModel.swift
//  20230303-QasimAlmahammedi-NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import SwiftUI

// MARK :- SchoolDetailsView.ViewModel
extension SchoolDetailsView {
    @MainActor class ViewModel: ObservableObject {
        @Published var isLoading = false

        func loadDetails(for id: String) async -> School.Details? {
            isLoading = true
            defer { isLoading = false }
            let resource = SchoolDetailsResource(id: id)
            let request = APIRequest(resource: resource)
            guard let details = await request.execute() else { return nil }
            return details.first
        }
    }
}

