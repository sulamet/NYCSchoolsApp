//
//  SchoolDetailsView.swift
//  20230303-QasimAlmahammedi-NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import SwiftUI

struct SchoolDetailsView: View {
    @StateObject private var viewModel = SchoolDetailsView.ViewModel()
    private var id: String
    @State private var details: School.Details?
    @State private var isErrorAlertPresented = false

    init(id: String) {
        self.id = id
    }

    var body: some View {
        Content(details: $details)
            .loading(viewModel.isLoading)
            .task {
                guard let details = await viewModel.loadDetails(for: id) else {
                    isErrorAlertPresented = true
                    return
                }
                self.details = details
            }
            .alert(isPresented: $isErrorAlertPresented) {
                Alert(title: Text("There was an error fetching school details."), message: Text("Please try again later"))
            }

    }
}

extension SchoolDetailsView {
    struct Content: View {
        @Binding var details: School.Details?

        var body: some View {
            ScrollView(.vertical) {
                LazyVStack(alignment: .leading) {
                    if let details = details {
                        VStack(alignment: .leading, spacing: 8.0) {
                            Text(details.name)
                                .font(.headline)
                            Group {
                                Text("Test takers: \(details.testTakers)")
                                Text("Math score: \(details.mathScore)")
                                Text("Reading score: \(details.readingScore)")
                                Text("Writing score: \(details.writingScore)")
                            }
                            .font(.subheadline)

                            ZStack(alignment: Alignment(horizontal: .leading, vertical: .center)) {
                                Label("\(details.id)", systemImage: "tag")
                            }
                            .foregroundColor(.teal)
                            .padding(.top, 16)
                        }
                    }
                }
                .padding(.horizontal, 20.0)
            }
            .navigationTitle(details?.id ?? "")

        }
    }
}

extension SchoolDetailsView {
    func load(_ id: String) async {
        guard let item = await viewModel.loadDetails(for: id) else {
            return
        }
        self.details = item
    }
}


struct SchoolDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailsView(id: "28473")
    }
}
