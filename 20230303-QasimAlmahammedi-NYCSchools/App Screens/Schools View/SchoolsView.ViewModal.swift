//
//  SchoolsView.ViewModal.swift
//  NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import Foundation

extension SchoolsView {
    @MainActor class ViewModel: ObservableObject {
        @Published var isLoading = false

        func fetchAllSchools() async -> [School]? {
            isLoading = true
            defer { isLoading = false }
            let resource = SchoolsResource()
            let request = APIRequest(resource: resource)
            return await request.execute()
        }
    }
}
