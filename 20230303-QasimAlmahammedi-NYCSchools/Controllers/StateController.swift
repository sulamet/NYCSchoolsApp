//
//  StateController.swift
//  20230303-QasimAlmahammedi-NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import Foundation
import UIKit

class StateController: ObservableObject {

    @Published var topSchools: [School] {
        didSet { storageController.save(topSchools: topSchools) }
    }

    subscript(schoolID: String) -> School {
        get { topSchools[index(for: schoolID)] }
        set { topSchools[index(for: schoolID)] = newValue }
    }

    private let storageController = StorageController()

    init() {
        topSchools = storageController.fetchTopSchools() ?? []
    }

}

private extension StateController {
    func index(for schoolID: String) -> Int {
        topSchools.firstIndex(where: { $0.id == schoolID })!
    }
}

