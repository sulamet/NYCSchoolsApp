//
//  SettingsController.swift
//  20230303-QasimAlmahammedi-NYCSchools
//
//  Created by Qasim Al Mahammedi on 3/15/23.
//

import Foundation

class SettingsController: ObservableObject {
    @Published var theme: Theme {
        didSet { defaults.set(theme.name, forKey: NYCSchoolsApp.Keys.themeName) }
    }

    private let defaults = UserDefaults.standard

    init() {
        let themeName = defaults.string(forKey: NYCSchoolsApp.Keys.themeName) ?? ""
        theme = Theme.named(themeName) ?? .default
    }
}

